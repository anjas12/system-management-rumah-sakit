﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SytemManagementRumahSakit
{
   public class StaffInformation : Entity
    {
        public override string Id { get; set; }
        public virtual string Nama { get; set; }
        public virtual string Jenis_Kelamin { get; set; }
        public virtual string Alamat { get; set; }
        public virtual string Jabatan { get; set; }
        
    }   
}

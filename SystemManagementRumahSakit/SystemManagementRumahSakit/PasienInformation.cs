﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemManagementRumahSakit
{
    public class PasienInformation : Entity
    {
        public override string Id { get; set; }
        public virtual string Nama { get; set; }
        public virtual string Jenis_Kelamin { get; set; }
        public virtual string Alamat { get; set; }
        public virtual string Umur { get; set; }
        public virtual string Penyakit { get; set; }
    }
}

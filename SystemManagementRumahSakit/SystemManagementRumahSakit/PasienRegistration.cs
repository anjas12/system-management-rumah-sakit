﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemManagementRumahSakit
{
    public class PasienRegistration : Entity
    {
        public override string Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string JenisKelamin { get; set; }
        public virtual string Alamat { get; set; }
    }
}
